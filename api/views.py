import logging
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from .services import check_mailing_status

from api.models import Mailing, Client, Message
from api.serializers import MailingSerializer, MailingStatsSerializer, \
    ClientSerializer, MessageSerializer

mailings_logger = logging.getLogger('mailings')
clients_logger = logging.getLogger('clients')


# Mailing
class MailingCreateView(generics.CreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def perform_create(self, serializer):
        mailing = serializer.save()

        mailings_logger.info(f'Mailing with id {mailing.pk} has been created')

        check_mailing_status(mailing)


class MailingRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    lookup_field = 'pk'

    def perform_update(self, serializer):
        mailing = serializer.save()

        mailings_logger.info(f'Mailing with id {mailing.pk} has been updated')

        check_mailing_status(mailing)

    def perform_destroy(self, instance):
        mailings_logger.info(f'Mailing with id {instance.pk} has been deleted')

        instance.delete()


# Mailing Stats
class MailingListStats(APIView):
    def get(self, request):
        queryset = Mailing.objects.all()
        serializer = MailingStatsSerializer(queryset, many=True)
        return Response(serializer.data)


class MailingDetailStats(APIView):
    def get(self, request, pk):
        queryset = Message.objects.filter(mailing=pk)
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)


# Client
class ClientListCreateView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def perform_create(self, serializer):
        client = serializer.save()

        clients_logger.info(f'Client with id {client.pk} has been created')


class ClientRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    lookup_field = 'pk'

    def perform_update(self, serializer):
        client = serializer.save()

        clients_logger.info(f'Client with id {client.pk} has been updated')

    def perform_destroy(self, instance):
        clients_logger.info(f'Client with id {instance.pk} has been deleted')

        instance.delete()
