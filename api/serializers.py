from rest_framework import serializers
from api.models import Mailing, Client, Message


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = '__all__'


class MailingStatsSerializer(serializers.ModelSerializer):
    delivered_messages = serializers.SerializerMethodField('delivered')
    undelivered_messages = serializers.SerializerMethodField('undelivered')

    def delivered(self, pk):
        return Message.objects.filter(mailing=pk, is_delivered=True).count()

    def undelivered(self, pk):
        return Message.objects.filter(mailing=pk, is_delivered=False).count()

    class Meta:
        model = Mailing
        fields = ('id', 'text',
                  'phone_code_filter', 'tag_filter',
                  'start_date', 'end_date',
                  'delivered_messages', 'undelivered_messages',
                  )


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
