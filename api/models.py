import pytz
from django.db import models
from django.core.validators import RegexValidator, MinLengthValidator
from django.utils import timezone


class Mailing(models.Model):
    MESSAGE = 'Телефонный код должен содержать только цифры от 0 до 9'

    text = models.TextField(null=False)

    # Filters
    phone_code_filter = models.CharField(
        validators=[RegexValidator(regex='^[0-9]+$', message=MESSAGE)],
        max_length=16,
        null=False)
    tag_filter = models.CharField(max_length=256, null=False)

    # Date interval of Mailing
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=False)

    class Meta:
        verbose_name = 'рассылка'
        verbose_name_plural = 'рассылки'

    def __str__(self):
        return f'Текст: {self.text[:75] + "..." if len(self.text) > 75 else self.text},' \
               f'Тел. код: {self.phone_code_filter}, Тэг: {self.tag_filter} | ' \
               f'Начало: {self.start_date.strftime(("%d.%m.%Y %H:%M:%S"))}, ' \
               f'Конец: {self.end_date.strftime(("%d.%m.%Y %H:%M:%S"))}'

    def is_active(self):
        return self.start_date < timezone.localtime(
            timezone.now()) < self.end_date

    def is_scheduled(self):
        return self.start_date > timezone.localtime(timezone.now())


class Client(models.Model):
    TIME_ZONES = pytz.country_timezones['ru']
    TIME_ZONES = tuple(zip(TIME_ZONES, TIME_ZONES))
    MESSAGE = \
        'Телефонный номер должен быть в формате 7XXXXXXXXXX (X - цифра от 0 до 9)'
    CODE_MESSAGE = 'Телефонный код должен содержать только цифры от 0 до 9'

    phone_number = models.CharField(
        validators=[RegexValidator(regex='^(7)\d{10}', message=MESSAGE),
                    MinLengthValidator(11)],
        max_length=11,
        unique=True)
    phone_code = models.CharField(
        validators=[RegexValidator(regex='^[0-9]+$', message=CODE_MESSAGE)],
        max_length=16)

    tag = models.CharField(max_length=256)
    time_zone = models.CharField(
        choices=TIME_ZONES,
        default='Europe/Moscow',
        max_length=64)

    class Meta:
        verbose_name = 'клиент'
        verbose_name_plural = 'клиенты'

    def __str__(self):
        return f'Тел. номер: {self.phone_number}, ' \
               f'Тел. код: {self.phone_code} |  ' \
               f'Тэг: {self.tag}, Часовой пояс: {self.time_zone}'


class Message(models.Model):
    date = models.DateTimeField(default=timezone.now)
    is_delivered = models.BooleanField(default=False)

    # Relationships
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения'

    def __str__(self):
        return f'Время отправки: {self.date.strftime(("%d.%m.%Y %H:%M:%S"))}, ' \
               f'{"Доставлено" if self.is_delivered else "Не доставлено"}, ' \
               f'Рассылка: {self.mailing.text} | ' \
               f'Клиент: {self.client.phone_number}'
