from django.urls import path
from . import views


urlpatterns = [
    # Mailing URLs
    path('mailings', views.MailingCreateView.as_view()),
    path('mailings/<int:pk>',
         views.MailingRetrieveUpdateDestroyView.as_view()),

    # Stats URLs
    path('mailings/<int:pk>/stats',
         views.MailingDetailStats.as_view()),
    path('mailings/stats', views.MailingListStats.as_view()),

    # Clients URLs
    path('clients', views.ClientListCreateView.as_view()),
    path('clients/<int:pk>', views.ClientRetrieveUpdateDestroyView.as_view()),
]
