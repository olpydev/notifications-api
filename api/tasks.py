import logging
import requests
import time
from celery import shared_task
from django.conf import settings

from api.models import Mailing, Client, Message

logger = logging.getLogger('messages')

endpoint = settings.API_ENDPOINT
headers = {"Authorization": f"Bearer {settings.API_TOKEN}"}


@shared_task
def send_messages(pk, phone_code_filter, tag_filter):
    clients = Client.objects.filter(phone_code=phone_code_filter,
                                    tag=tag_filter)
    mailing = Mailing.objects.get(pk=pk)

    for client in clients:
        message = Message(mailing=mailing,
                          client=client)
        message.save()

        logger.info(f'Message with id {message.pk} has been created '
                    f'and will be sent to the client with id {client.pk}')

        url = endpoint + str(message.pk)
        json = {"id": message.pk,
                "phone": int(message.client.phone_number),
                "text": message.mailing.text}

        # While loop variables
        sending = True
        timeout = 15

        while sending:
            try:
                logger.info('Calling external API')

                response = requests.post(url=url,
                                         json=json,
                                         headers=headers,
                                         timeout=3)
                response.raise_for_status()

                message.is_delivered = True
                message.save()

                logger.info(f'Message with id {message.pk} has been delivered')

                sending = False
            except (requests.HTTPError, requests.Timeout) as exc:
                time.sleep(timeout)
                timeout *= 2

                logger.error(
                    f'External API is not working properly. Error: {exc}')
