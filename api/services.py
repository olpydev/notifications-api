import logging
from .tasks import send_messages

from .models import Mailing

logger = logging.getLogger('mailings')


def check_mailing_status(mailing: Mailing):
    if mailing.is_active():
        logger.info(
            f'Mailing with id {mailing.pk} will be performed right now')

        task = send_messages.delay(mailing.pk,
                                   mailing.phone_code_filter,
                                   mailing.tag_filter)
        logger.info(
            f'Task {task} has been created and will be performed right now')

    if mailing.is_scheduled():
        logger.info(
            f'Mailing with id {mailing.pk} will be performed {mailing.start_date}')

        task = send_messages.apply_async(eta=mailing.start_date,
                                         args=[mailing.pk,
                                               mailing.phone_code_filter,
                                               mailing.tag_filter])

        logger.info(
            f'Task {task} has been created and will be performed {mailing.start_date}')
