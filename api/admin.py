from django.contrib import admin
from api.models import Mailing, Message, Client

# Register your models here.
admin.site.register(Mailing)
admin.site.register(Client)
admin.site.register(Message)
