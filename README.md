# Notifications API

Notification API where you can create and manage clients and mailings, create and send messages to the clients via external API. Here is little demonstration:

![Demonstration Gif](Animation.gif)


## Using Application

1. Clone repository.
2. Install requirements: `pip3 install -r requirements.txt`
3. Make migrations first: `python manage.py makemigrations api`
4. Run initial migration: `python manage.py migrate`
5. Create superuser for Django Admin Panel: `python manage.py createsuperuser`
6. Run Django Server: `python manage.py runserver`
7. Run Celery Worker: `celery -A notifications worker --pool=solo --loglevel=INFO`


## Application Details

- Swagger-UI documentation lies here: *http://127.0.0.1:8000/docs/*
![How it looks like](https://sun9-44.userapi.com/impg/65y5O9dp9-0oBQ44Sxwg8XFgUjFX6VousvI_bA/Qzc3ehdlpns.jpg?size=1434x822&quality=96&sign=9123ee9b153a50b7103c4522da0f9c3c&type=album)

- Logger files lie in: *notifications-app/notifications/logs/{name of a logger}*







