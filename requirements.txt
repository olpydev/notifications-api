amqp==5.0.9
asgiref==3.5.0
billiard==3.6.4.0
celery==5.2.3
certifi==2021.10.8
charset-normalizer==2.0.12
click==8.0.4
click-didyoumean==0.3.0
click-plugins==1.1.1
click-repl==0.2.0
colorama==0.4.4
Deprecated==1.2.13
Django==4.0.2
django-celery-results==2.2.0
djangorestframework==3.13.1
idna==3.3
kombu==5.2.3
packaging==21.3
prompt-toolkit==3.0.28
pyparsing==3.0.7
pytz==2021.3
PyYAML==6.0
redis==4.1.4
requests==2.27.1
six==1.16.0
sqlparse==0.4.2
tzdata==2021.5
uritemplate==4.1.1
urllib3==1.26.8
vine==5.0.0
wcwidth==0.2.5
wrapt==1.13.3
